export const USERS = [
    {
        id: 1,
        firstName: "Hovhannes",
        lastName: "Sargsyan"
    },
    {
        id: 2,
        firstName: "Aram",
        lastName: "Grigoryan"
    },
    {
        id: 3,
        firstName: "Ani",
        lastName: "Khachatryan"
    },
    {
        id: 4,
        firstName: "Gor",
        lastName: "Manukyan"
    },
    {
        id: 5,
        firstName: "Nelli",
        lastName: "Vardanyan"
    }
]
