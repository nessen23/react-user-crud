import './App.css';
import Users from './components/Users.jsx';
import { USERS } from './data/usersData';

function App() {
  return (
    <Users users = {USERS} />
  );
}

export default App;
