import React, { useState, useEffect } from 'react';
import { Modal, Button } from 'react-bootstrap';
import PropTypes from 'prop-types';
import UserForm from './UserForm.jsx';

function ModalDialog(props) {
  const { show, action, user, users, modalshow } = props;

  const [firstNameError, setFirstNameError] = useState('');
  const [lastNameError, setLastNameError] = useState('');
  const [errors, setErrors] = useState({ firstNameError, lastNameError })

  useEffect(() => {
    setErrors({ firstNameError, lastNameError })
  }, [firstNameError, lastNameError])

  useEffect(() => {
    if(show) {
      setFirstNameError('');
      setLastNameError('');
    }
  }, [show])

  const handleSave = (action) => {
    if(action === 'Edit') {
      const currentUser = users.filter((item) => item.id === user.id)[0];

      if(user.firstName) {
        currentUser.firstName = user.firstName;
        setFirstNameError('');
      } else {
        setFirstNameError('First name is required');
      }

      if(user.lastName) {
        currentUser.lastName = user.lastName;
        setLastNameError('');
      } else {
        setLastNameError('Last name is required');
      }

      if(user.firstName && user.lastName) {
        props.onHide();
        setTimeout(() => {
          user.firstName = '';
          user.lastName = '';
        }, 500)
      }
    } else if(action === 'Add') {
      if(user.firstName && user.lastName) {
        users.push({
          id: users[users.length - 1].id + 1,
          firstName: user.firstName,
          lastName: user.lastName
        });
        props.onHide();
        setTimeout(() => {
          user.firstName = '';
          user.lastName = '';
        }, 500)
      }
      
      if(!user.firstName) {
        setFirstNameError('First name is required');
      } else {
        setFirstNameError('')
      }

      if(!user.lastName) {
        setLastNameError('Last name is required');
      } else {
        setLastNameError('')
      }
    }
  };

  return (
    <Modal
      {...props}
      size="lg"
      aria-labelledby="contained-modal-title-vcenter"
      centered
    >
      <Modal.Header closeButton>
        <Modal.Title id="contained-modal-title-vcenter">
          {action === 'Edit' ? 'Edit user' : 'Create user'}
        </Modal.Title>
      </Modal.Header>
      <Modal.Body>
        <UserForm userData={user} action={action} errors={errors} />
      </Modal.Body>
      <Modal.Footer>
        <Button onClick={props.onHide}>Cancel</Button>
        <Button variant="primary" onClick={() => handleSave(action)}>{action === 'Edit' ? 'Save Changes' : 'Add'}</Button>
      </Modal.Footer>
    </Modal>
  );
};

ModalDialog.propTypes = {
  action: PropTypes.string.isRequired,
  user: PropTypes.object,
};

export default ModalDialog;
