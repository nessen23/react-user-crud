import React, { useState } from 'react';
import { Table, Button } from 'react-bootstrap';
import PropTypes from 'prop-types';
import ModalDialog from './ModalDialog.jsx';

const Users = ({users}) => {
  const [modalShow, setModalShow] = useState(false);
  const [action, setAction] = useState('');
  const [currentUserData, setCurrentUserData] = useState({ firstName: '', lastName: '' });
  const [usersCopy, setUsersCopy] = useState(users);

  const deleteUser = (id) => {
    setUsersCopy(usersCopy.filter((user) => {
      return user.id !== id;
    }));
  };

  const openAddUserDialog = () => {
    setModalShow(true);
    setAction('Add');
  }

  const openEditUserDialog = (id) => {
    const user = usersCopy.filter((user) => user.id === id)[0];
    setCurrentUserData({
      id,
      firstName: user.firstName,
      lastName: user.lastName
    })
    setModalShow(true);
    setAction('Edit')
  }

  return (
    <div style={{width: '90%', margin: '50px auto 0'}}>
      <Button variant="success" onClick={() => openAddUserDialog()} style={{marginBottom: '10px'}}>Add User</Button>
      <Table responsive striped bordered hover variant="dark">
        <thead>
          <tr>
            <th>#</th>
            <th>First Name</th>
            <th>Last Name</th>
            <th>Actions</th>
          </tr>
        </thead>
        <tbody>
          {
            usersCopy.map((user, index) => {
              return (
                <tr key={index.toString()}>
                  <td>{user.id}</td>
                  <td>{user.firstName}</td>
                  <td>{user.lastName}</td>
                  <td style={{width: '200px'}}>
                    <Button
                      variant="outline-info" 
                      style={{ marginRight: 10 }}
                      onClick={() => openEditUserDialog(user.id)}
                    >
                        Edit
                    </Button>
                    <Button
                      variant="outline-danger"
                      onClick={e =>
                        window.confirm("Are you sure you want to delete this user?") &&
                        deleteUser(user.id)
                      }
                    >
                      Delete
                    </Button>

                  </td>
                </tr>
              )
            })
          }
        </tbody>
      </Table>
      <ModalDialog
        show={modalShow}
        onHide={() => setModalShow(false)}
        action={action}
        user={currentUserData}
        users={usersCopy}
      />
    </div>
  )
};

Users.propTypes = {
  users: PropTypes.arrayOf(
    PropTypes.shape({
      id: PropTypes.number,
      firstName: PropTypes.string,
      lastName: PropTypes.string
    })
  ).isRequired,
};

export default Users;
