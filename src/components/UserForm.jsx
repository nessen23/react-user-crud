import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import { InputGroup, FormControl } from 'react-bootstrap';

const UserForm = (props) => {
  const { userData, action, errors } = props;

  const [firstName, setFirstName] = useState(userData.firstName);
  const [lastName, setLastName] = useState(userData.lastName);

  useEffect(() => {
    if(action === 'Add') {
      setFirstName('');
      setLastName('')
    }
  }, [action])

  const onChangeHandler = (e) => {
    if(e.target.name === 'firstName') {
      setFirstName(e.target.value);
      userData.firstName = e.target.value;
    } else if(e.target.name === 'lastName') {
      setLastName(e.target.value);
      userData.lastName = e.target.value
    }
  }
  return (
    <>
      <InputGroup className="mb-3">
        <FormControl
          placeholder="First Name"
          aria-label="First Name"
          aria-describedby="basic-addon1"
          name="firstName"
          value={firstName}
          onChange={(e) => onChangeHandler(e)}
        />
      </InputGroup>
      {errors.firstNameError && <p style={{marginTop: '-15px', color: 'red'}}>{errors.firstNameError}</p>}

      <InputGroup className="mb-3">
        <FormControl
          placeholder="Last Name"
          aria-label="Last Name"
          aria-describedby="basic-addon1"
          name="lastName"
          value={lastName}
          onChange={(e) => onChangeHandler(e)}
        />
      </InputGroup>
      {errors.lastNameError && <p style={{marginTop: '-15px', color: 'red'}}>{errors.lastNameError}</p>}
    </>
  )
};

UserForm.propTypes = {
  action: PropTypes.string,
  userData: PropTypes.object,
  errors: PropTypes.object
};

export default UserForm;
